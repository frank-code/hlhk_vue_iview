const Storage = sessionStorage

export default{
    set(key,val){
        Storage.setItem(key,val)
    },
    get(key){
       return Storage.getItem(key)
    },
    remove(key){
        Storage.removeItem(key);
    },
}