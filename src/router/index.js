import Vue from 'vue'
import VueRouter from 'vue-router'
import util from '@/util'

Vue.use(VueRouter)

const router = new VueRouter({

  routes: [
    {
      path:'/test',
      name: 'test',
      component: () => import('@/views/test.vue'),
      meta:{
        title:"测试"
      }
    },
    {
      path:'/login',
      name: 'login',
      component: () => import('@/views/user/login.vue'),
      meta:{
        title:"登陆",
        order:"1-1"
      }
    },
    {
      path:'/regist',
      name: 'regist',
      component: () => import('@/views/user/registered.vue'),
      meta:{
        title:"注册",
        order:"1-1"
      }
    },

    // 普通 角色登陆
    {
      path: '/',
      name: 'home',
      component:() => import('@/views/main.vue'),
      children:[
        {
          path:'',
          component: () => import('@/views/home.vue'),
          meta:{
            title:"主页"
          }
        },
        {
          // 设备
          path:'/Device',
          component: () => import('@/views/Other/ConstructionLedger.vue'),
          meta:{
            title:"设备台账",
            order:"2-1"
          }
        },
        {
          // 建筑
          path:'/Factor',
          component: () => import('@/views/Other/device.vue'),
          meta:{
            title:"建筑台账",
            order:"2-2"
          }
        },
        {
          // 油管
          path:'/Tubing',
          component: () => import('@/views/Other/PL_Tubing.vue'),
          meta:{
            title:"油管",
            order:"2-3"
          }
        }, 
        {
          // 煤气罐
          path:'/Gaspipe',
          component: () => import('@/views/Other/PL_Gaspipe.vue'),
          meta:{
            title:"煤气管线",
            order:"2-4"
          }
        }, 
        {
          // 污水管线
          path:'/Sewage',
          component: () => import('@/views/Other/PL_Sewage.vue'),
          meta:{
            title:"污水管线",
            order:"2-5"
          }
        }, 
        
        
      ]

    },
    {
      path: '/manager',
      name: '管理员界面',
      component:() => import('@/views/manager/manager_main.vue'),
      children:[
        {
          // 用户列表
          path:'/userlist',
          component: () => import('@/views/manager/U_userlist.vue'),
          meta:{
            title:"用户列表",
            order:"1-1"
          }
        },
        {
          // 创建用户
          path:'/createUser',
          component: () => import('@/views/manager/U_CreateUser.vue'),
          meta:{
            title:"创建用户",
            order:"1-2"
          }
        },
        {
          // 权限列表
          path:'/role-list',
          component: () => import('@/views/manager/U_RightsProfile.vue'),
          meta:{
            title:"权限配置",
            order:"1-3"
          }
        },
        {
          // 角色
          path:'/role',
          component: () => import('@/views/manager/role.vue'),
          meta:{
            title:"权限配置",
            order:"1-2"
          }
        },
        {
          // 管理后台建筑表
          path:'/CE_Houses',
          component: () => import('@/views/manager/CE_Houses.vue'),
          meta:{
            title:"管理后台建筑表",
            order:"2-1"
          }
        },
        {
          // 管理后台设备表
          path:'/CE_Device',
          component: () => import('@/views/manager/CE_Device.vue'),
          meta:{
            title:"管理后台设备表",
            order:"2-2"
          }
        },
        {
          // 油管
          path:'/PL_Tubing',
          component: () => import('@/views/manager/PL_Tubing.vue'),
          meta:{
            title:"油管",
            order:"3-1"
          }
        }, 
        {
          // 煤气罐
          path:'/PL_Gaspipe',
          component: () => import('@/views/manager/PL_Gaspipe.vue'),
          meta:{
            title:"煤气管线",
            order:"3-2"
          }
        }, 
        {
          // 污水管线
          path:'/PL_Sewage',
          component: () => import('@/views/manager/PL_Sewage.vue'),
          meta:{
            title:"污水管线",
            order:"3-3"
          }
        }, 

        {
          // 设备跟新
          path:'/Update_Device',
          component: () => import('@/views/manager/Update_Device.vue'),
          meta:{
            title:"设备跟新",
            order:"4-1"
          }
        }, 

        {
          // 建筑跟新
          path:'/Update_Houses',
          component: () => import('@/views/manager/Update_Houses.vue'),
          meta:{
            title:"建筑跟新",
            order:"4-2"
          }
        }, 



      ]
    }
  ]
})

export default router


var vm = new Vue()


router.beforeEach((to, from, next) => {

 
  if( to.path == '/login'  ||
      to.path == '/regist' ||
      to.path == '/Device' ||
      to.path == '/Factor')
  {
      vm.$Loading.start();
      if(to.meta && to.meta.title){
        document.title = "HLHK - " + to.meta.title;
      }
      return next()
  }

  //  // 判断是否登录
  if(!util.storage.get('LoginUser'))
  {
     vm.$Message.warning('检测到您还未登录,请登录后操作！');
     return next('/login')
  }
  
  next()

})
router.afterEach((to, from)=>{
  vm.$Loading.finish();

})