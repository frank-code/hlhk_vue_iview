// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import iView from 'iview'
import 'iview/dist/styles/iview.css'

import qs from 'qs';
// axios
import axios from 'axios';

import {Spin} from 'iview'

// axios方法绑定到全局
Vue.prototype.$http = axios;

Vue.prototype.$qs = qs;
Vue.prototype.$Spin = Spin

Vue.use(iView)
Vue.config.productionTip = false
// IP
Vue.prototype.$IP = 'http://128.128.10.83:8000/';
// 登陆平台   PC 和 WEB
Vue.prototype.platform ='PC'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  data:function(){
    return{
      IP:'http://127.0.0.1:8000/',
      platform:'PC'

    }
    
},
})
